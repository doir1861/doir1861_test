package salariati.controller;

import java.util.List;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}

	public Employee findEmployee(String CNP) {
		Employee employee = employeeRepository.findEmployee(CNP);
		return employee;
	}

	public List<Employee> getEmployeeByCriteria(String criteria) {
		return employeeRepository.getEmployeeByCriteria(criteria);
	}

	public void clearRepo() {
		employeeRepository.clearRepo();
	}
}
