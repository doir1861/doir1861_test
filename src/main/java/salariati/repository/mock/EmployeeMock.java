package salariati.repository.mock;

import java.util.ArrayList;
import java.util.List;

import salariati.enumeration.DidacticFunction;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Ionel   = new Employee("Ionel Pacuraru", "1234567890871", DidacticFunction.ASISTENT, "2500");
		Employee Mihai   = new Employee("Mihai Dumitrescu", "1234567890872", DidacticFunction.LECTURER, "2500");
		Employee Ionela  = new Employee("Ionela Ionescu", "1234567890873", DidacticFunction.LECTURER, "2500");
		Employee Mihaela = new Employee("Mihaela Pacuraru", "1234567890874", DidacticFunction.ASISTENT, "2500");
		Employee Vasile  = new Employee("Vasile Georgescu", "1234567890875", DidacticFunction.TEACHER,  "2500");
		Employee Marin   = new Employee("Marin Puscas", "1234567890876", DidacticFunction.CONFERENTIAR,  "2500");
		
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		int i = 0;

		if (employee == null) {
			return;
		}
		while(i < employeeList.size()) {
			if(employeeList.get(i).getCnp().equals(employee.getCnp())) {
				employeeList.remove(employeeList.get(i));
			} else {
				i++;
			}
		}
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		if ( employeeValidator.isValid(newEmployee)) {
			int count = employeeList.size();
			deleteEmployee(oldEmployee);
			int afterCount = employeeList.size();
			if (count != afterCount) {
				addEmployee(newEmployee);
			}

		}

	}

	@Override
	public Employee findEmployee(String CNP) {
		int i = 0;
		while(i < employeeList.size()) {
			if(employeeList.get(i).getCnp().equals(CNP)) {
				return employeeList.get(i);
			} else {
				i++;
			}
		}
		return null;
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	public void clearRepo() {
		employeeList.clear();
	}

}
