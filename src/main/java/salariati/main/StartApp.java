package salariati.main;

import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

import java.util.List;
import java.util.Scanner;

import static salariati.enumeration.DidacticFunction.*;

//functionalitati
//		a) adaugarea unui nou angajat (nume, prenume, CNP, functia didactica care poate fi de asistent/lector/conferentiar/profesor, salariul);
//		b) modificarea unui angajat;
//		c) stergerea unui angajat;
//		d) afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {
	
	public static void main(String[] args) {

            EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
            EmployeeController employeeController = new EmployeeController(employeesRepository);

            int cmd = 1;
            Scanner in = new Scanner(System.in);

            while(true) {
                    printMenu();

                    cmd = in.nextInt();

                    if (cmd == 1)
                            printEmployees(employeeController.getEmployeesList());
                    else if (cmd == 2) {
                            System.out.println("NAME: ");
                            String name = in.next();
                            System.out.println("CNP: ");
                            String CNP = in.next();
                            System.out.println("SALARY: ");
                            String salary = in.next();
                            System.out.println("DIDACTIC FUNCTION: ");
                            String didacticFunction = in.next();
                            if (didacticFunction.equals("asistent")) {
                                    Employee empl   = new Employee(name, CNP, ASISTENT, salary);
                                    employeeController.addEmployee(empl);
                            } else if (didacticFunction.equals("lecturer")) {
                                    Employee empl   = new Employee(name, CNP, LECTURER, salary);
                                    employeeController.addEmployee(empl);
                            } else if (didacticFunction.equals("teacher")) {
                                    Employee empl   = new Employee(name, CNP, TEACHER, salary);
                                    employeeController.addEmployee(empl);
                            } else if (didacticFunction.equals("conferentiar")) {
                                    Employee empl   = new Employee(name, CNP, CONFERENTIAR, salary);
                                    employeeController.addEmployee(empl);
                            }
                    } else if (cmd == 3) {
                            System.out.println("CNP: ");
                            String CNP = in.next();
                            Employee found = employeeController.findEmployee(CNP);
                            employeeController.deleteEmployee(found);
                    } else if (cmd == 4) {
                            System.out.println("OLD EMPLOYEE CNP: ");
                            String oldCNP = in.next();
                            System.out.println("NEW LAST NAME: ");
                            String name = in.next();
                            System.out.println("NEW CNP: ");
                            String CNP = in.next();
                            System.out.println("NEW SALARY: ");
                            String salary = in.next();
                            System.out.println("NEW DIDACTIC FUNCTION: ");
                            String didacticFunction = in.next();

                            Employee found = employeeController.findEmployee(oldCNP);

                            if (didacticFunction.equals("asistent")) {
                                    Employee employee = new Employee(name, CNP, ASISTENT, salary);
                                    employeeController.modifyEmployee(found, employee);
                            } else if (didacticFunction.equals("lecturer")) {
                                    Employee employee = new Employee(name, CNP, LECTURER, salary);
                                    employeeController.modifyEmployee(found, employee);
                            } else if (didacticFunction.equals("teacher")) {
                                    Employee employee = new Employee(name, CNP, TEACHER, salary);
                                    employeeController.modifyEmployee(found, employee);
                            } else if (didacticFunction.equals("conferentiar")) {
                                    Employee employee = new Employee(name, CNP, CONFERENTIAR, salary);
                                    employeeController.modifyEmployee(found, employee);
                            }

                    } else if (cmd == 5) {
                            List<Employee> newList = employeeController.getEmployeeByCriteria("Salary");
                            printEmployees(newList);
                    } else if (cmd == 6) {
                            List<Employee> newList = employeeController.getEmployeeByCriteria("CNP");
                            printEmployees(newList);
                    } else {
                            break;
                    }
            }
    }

        private static void printMenu() {
                System.out.println("----------MENU----------");
                System.out.println("1. PRINT EMPLOYEES");
                System.out.println("2. ADD EMPLOYEE");
                System.out.println("3. DELETE EMPLOYEE");
                System.out.println("4. UPDATE EMPLOYEE");
                System.out.println("5. SORT REVERSED BY SALARY");
                System.out.println("6. SORT BY CNP");
                System.out.println("7. EXIT");
        }

        private static void printEmployees(List<Employee> employeeList) {
                for(Employee _employee : employeeList)
                        System.out.println(_employee.toString());
                System.out.println("-----------------------------------------");
        }
}
