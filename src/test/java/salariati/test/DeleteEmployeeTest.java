package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.*;

public class DeleteEmployeeTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() throws Exception {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }
    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void deleteEmployee() {
        // Test1 valid
        Employee e1 = controller.findEmployee("1234567890871");
        controller.deleteEmployee(e1);
        assertEquals(5, controller.getEmployeesList().size());

        // Test2 invalid
        Employee e2 = controller.findEmployee("1234567890879");
        controller.deleteEmployee(e2);
        assertEquals(5, controller.getEmployeesList().size());
    }
}