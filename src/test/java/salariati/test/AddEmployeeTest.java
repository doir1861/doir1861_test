package salariati.test;

import static org.junit.Assert.*;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMock();
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();
	}
	
	@Test
	public void testRepositoryMock() {
		assertFalse(controller.getEmployeesList().isEmpty());
		assertEquals(6, controller.getEmployeesList().size());
	}
	
	@Test
	public void testAddNewEmployee() {

	    //Test 1
		Employee newEmployee = new Employee("Octavian Duminica", "1960428055087", DidacticFunction.ASISTENT, "2000");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(7, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

		//Test 2
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 3
        newEmployee = new Employee("12", "asd", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 4
        newEmployee = new Employee("12", "19604280", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 5
        newEmployee = new Employee("", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 6
        newEmployee = new Employee("12", "196042805508", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 7
        newEmployee = new Employee("12", "", DidacticFunction.ASISTENT, "-1500");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 8
        newEmployee = new Employee("Octavian Dum", "1960428055087", DidacticFunction.ASISTENT, "0");
        assertFalse(employeeValidator.isValid(newEmployee));
	}

	@Test // valid ECP
	public void Test1() {
		//Test 1
		Employee newEmployee = new Employee("Octavian Duminica", "1960428055087", DidacticFunction.ASISTENT, "2000");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(7, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test // invalid ECP
	public void Test2() {
		//Test 2
		Employee newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "2000");
		assertFalse(employeeValidator.isValid(newEmployee));
	}

	@Test // valid BVA
	public void Test3() {
		Employee newEmployee = new Employee("Octavian Duminica", "1960428055089", DidacticFunction.ASISTENT, "10");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(7, controller.getEmployeesList().size());
	}

	@Test // invalid BVA
	public void Test4() {
		//Test 8
		Employee newEmployee = new Employee("Octavian Dum", "1960428055087", DidacticFunction.ASISTENT, "0");
		assertFalse(employeeValidator.isValid(newEmployee));
	}

}
