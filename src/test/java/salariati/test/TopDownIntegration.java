package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TopDownIntegration {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void testModuleA() {

        //Test 1
        Employee newEmployee = new Employee("Octavian Duminica", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
        assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        //Test 2
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 3
        newEmployee = new Employee("12", "asd", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 4
        newEmployee = new Employee("12", "19604280", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 5
        newEmployee = new Employee("", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 6
        newEmployee = new Employee("12", "196042805508", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 7
        newEmployee = new Employee("12", "", DidacticFunction.ASISTENT, "-1500");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 8
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "0");
        assertFalse(employeeValidator.isValid(newEmployee));
    }

    @Test
    public void testModuleB() {
        //Test 1
        Employee newEmployee = new Employee("Octavian Duminica", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
        assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        //Test 2
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 3
        newEmployee = new Employee("12", "asd", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 4
        newEmployee = new Employee("12", "19604280", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 5
        newEmployee = new Employee("", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 6
        newEmployee = new Employee("12", "196042805508", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 7
        newEmployee = new Employee("12", "", DidacticFunction.ASISTENT, "-1500");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 8
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "0");
        assertFalse(employeeValidator.isValid(newEmployee));

        // Test1 valid
        Employee e1 = controller.findEmployee("1960428055087");
        controller.deleteEmployee(e1);
        assertEquals(6, controller.getEmployeesList().size());

        // Test2 invalid
        Employee e2 = controller.findEmployee("1234567890879");
        controller.deleteEmployee(e2);
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void testModuleC() {
        //Test 1
        Employee newEmployee = new Employee("Octavian Duminica", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(7, controller.getEmployeesList().size());
        assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        //Test 2
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 3
        newEmployee = new Employee("12", "asd", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 4
        newEmployee = new Employee("12", "19604280", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 5
        newEmployee = new Employee("", "1960428055087", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 6
        newEmployee = new Employee("12", "196042805508", DidacticFunction.ASISTENT, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 7
        newEmployee = new Employee("12", "", DidacticFunction.ASISTENT, "-1500");
        assertFalse(employeeValidator.isValid(newEmployee));

        //Test 8
        newEmployee = new Employee("12", "1960428055087", DidacticFunction.ASISTENT, "0");
        assertFalse(employeeValidator.isValid(newEmployee));

        // Test1 valid
        Employee e1 = controller.findEmployee("1960428055087");
        controller.deleteEmployee(e1);
        assertEquals(6, controller.getEmployeesList().size());

        // Test2 invalid
        Employee e2 = controller.findEmployee("1234567890879");
        controller.deleteEmployee(e2);
        assertEquals(6, controller.getEmployeesList().size());

        //Test 1
        controller.clearRepo();

        Employee Marin = new Employee("Marin Puscas", "1234567890876", DidacticFunction.CONFERENTIAR, "2500");
        controller.addEmployee(Marin);
        Employee newEmployee1 = new Employee("Octavian", "196042805508", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(Marin, newEmployee1);
        assertTrue(controller.getEmployeesList().get(0).getCnp().equals("1234567890876"));

        //Test 2
        Employee Mihai   = new Employee("Mihai Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "2500");
        Employee Ionela  = new Employee("Ionela Ionescu", "1224567890876", DidacticFunction.LECTURER, "2500");
        controller.addEmployee(Mihai);
        controller.addEmployee(Ionela);
        Employee newEmployee2 = new Employee("Octavian", "196042805508", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(Mihai, newEmployee2);
        assertTrue(controller.getEmployeesList().get(1).getCnp().equals("1234567890876"));


        //Test 3
        Employee newEmployee3 = new Employee("Octavian", "1960428055085", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(Mihai, newEmployee3);
        assertTrue(controller.getEmployeesList().get(1).getCnp().equals("1960428055085"));

        //Test 4
        Employee notFoundEmployee = new Employee("Barbu", "1960428055083", DidacticFunction.ASISTENT, "2000");
        Employee newEmployee4 = new Employee("Octavian", "1960428054085", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(notFoundEmployee, newEmployee4);
        assertTrue(controller.getEmployeesList().size() == 2);
    }

}
